﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zipbomb
{
    public partial class Form2 : Form
    {
        public string sign;

        public Form2(string s)
        {
            InitializeComponent();
            sign = s;
            analyseChar();
        }




        void analyseChar()
        {
            Dictionary<char, int> characterCount = new Dictionary<char, int>();
            foreach (char c in sign)
            {
                if (characterCount.ContainsKey(c))
                    characterCount[c]++;
                else
                    characterCount[c] = 1;
            }

            foreach (var pair in characterCount)
            {
                label1.Text += "\n" + "Character " + "\""+pair.Key + "\"" + " frequency: " + pair.Value;
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
